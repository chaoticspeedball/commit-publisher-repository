import org.junit.Test;

public class IntegrationTest {

    private static final boolean CRUSH_TESTS = false;





    @Test
    public void test() {
        System.out.println("Starting integration test...");

        doSomething();
    }

    private void doSomething() {
        if (CRUSH_TESTS) {
            System.out.println("Sorry, no way.");
            throw new RuntimeException("haha!");
        }
        else {
            System.out.println("You've got lucky this time.");
        }

        System.out.println("Looks like TeamCity is building projects from only from master branch.");
    }

}
